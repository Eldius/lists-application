package net.eldiosantos.spark.lists.repository.interfaces;

import java.io.Serializable;

public interface Delete<T, K extends Serializable> {
    void delete(T element);
}
