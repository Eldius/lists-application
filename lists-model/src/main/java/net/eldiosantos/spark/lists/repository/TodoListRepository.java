package net.eldiosantos.spark.lists.repository;


import net.eldiosantos.spark.lists.model.TodoList;
import net.eldiosantos.spark.lists.repository.interfaces.Repository;

import java.util.List;

public interface TodoListRepository extends Repository<TodoList, Long> {
    List<TodoList>getByOwner(final Long ownerId);
}
