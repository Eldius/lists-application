package net.eldiosantos.spark.lists.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/**
 * A simple to do list.
 */
public class TodoList implements Serializable {
    private Long id;
    private String displayName;

    private List<ListItem>items = new ArrayList<>();

    private Long ownerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<ListItem> getItems() {
        return new ArrayList<>(items);
    }

    public void setItems(Collection<ListItem> items) {
        this.items = new ArrayList<>(items);
    }

    public void clear() {
        items.clear();
    }

    public void forEach(Consumer<? super ListItem> action) {
        items.forEach(action);
    }

    public Iterator<ListItem> iterator() {
        return items.iterator();
    }

    public boolean add(ListItem listItem) {
        return items.add(listItem);
    }

    public boolean remove(Object o) {
        return items.remove(o);
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
