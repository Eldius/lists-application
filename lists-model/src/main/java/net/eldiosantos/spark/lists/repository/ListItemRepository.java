package net.eldiosantos.spark.lists.repository;

import net.eldiosantos.spark.lists.model.ListItem;
import net.eldiosantos.spark.lists.repository.interfaces.Repository;

public interface ListItemRepository extends Repository<ListItem, Long> {

}
