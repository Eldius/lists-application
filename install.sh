#!/bin/bash

sudo service app stop

mvn clean install

cp lists-spark/target/lists-spark-jar-with-dependencies.jar /vagrant/deploy

sudo service app start