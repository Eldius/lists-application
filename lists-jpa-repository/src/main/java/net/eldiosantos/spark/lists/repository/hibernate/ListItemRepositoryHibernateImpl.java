package net.eldiosantos.spark.lists.repository.hibernate;

import net.eldiosantos.spark.lists.model.ListItem;
import net.eldiosantos.spark.lists.repository.ListItemRepository;
import net.eldiosantos.spark.lists.repository.hibernate.base.BaseRepository;

import javax.persistence.EntityManager;

/**
 * Created by eldio.junior on 09/06/2015.
 */
public class ListItemRepositoryHibernateImpl extends BaseRepository<ListItem, Long> implements ListItemRepository {

    @Deprecated
    public ListItemRepositoryHibernateImpl() {
    }

    public ListItemRepositoryHibernateImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
