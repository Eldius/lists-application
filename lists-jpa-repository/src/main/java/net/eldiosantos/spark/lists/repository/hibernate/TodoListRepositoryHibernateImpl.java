package net.eldiosantos.spark.lists.repository.hibernate;

import net.eldiosantos.spark.lists.model.TodoList;
import net.eldiosantos.spark.lists.repository.TodoListRepository;
import net.eldiosantos.spark.lists.repository.hibernate.base.BaseRepository;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by eldio.junior on 09/06/2015.
 */
public class TodoListRepositoryHibernateImpl extends BaseRepository<TodoList, Long> implements TodoListRepository {

    @Deprecated
    public TodoListRepositoryHibernateImpl() {
    }

    public TodoListRepositoryHibernateImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<TodoList> getByOwner(final Long ownerId) {
        return entityManager.createQuery("select l from TodoList l where l.ownerId = :ownerId", TodoList.class)
                .getResultList();
    }
}
