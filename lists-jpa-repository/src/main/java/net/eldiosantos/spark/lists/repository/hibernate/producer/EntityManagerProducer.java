package net.eldiosantos.spark.lists.repository.hibernate.producer;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by eldio.junior on 09/06/2015.
 */
@ApplicationScoped
public class EntityManagerProducer {

    private EntityManagerFactory factory = Persistence.createEntityManagerFactory("service");

    @Produces
    @ApplicationScoped
    public EntityManager getEntityManager() {
        return factory.createEntityManager();
    }

    public void close(@Disposes final EntityManager entityManager) {
        entityManager.close();
    }
}
