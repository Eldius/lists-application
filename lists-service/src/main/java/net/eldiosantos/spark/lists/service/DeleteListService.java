package net.eldiosantos.spark.lists.service;

import net.eldiosantos.spark.lists.annotation.Transactional;
import net.eldiosantos.spark.lists.model.TodoList;
import net.eldiosantos.spark.lists.repository.TodoListRepository;

import javax.inject.Inject;

/**
 * Removes lists from database.
 */
public class DeleteListService {

    @Inject
    private TodoListRepository todoListRepository;

    @Transactional
    public void delte(final TodoList list) {
        todoListRepository.delete(list);
    }

    @Transactional
    public void delte(final Long listId) {
        delte(todoListRepository.getByPk(listId));
    }
}

