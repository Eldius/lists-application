package net.eldiosantos.spark.lists.service;

import net.eldiosantos.spark.lists.annotation.Transactional;
import net.eldiosantos.spark.lists.model.TodoList;
import net.eldiosantos.spark.lists.repository.TodoListRepository;

import javax.inject.Inject;

/**
 * Saves lists to database.
 */
public class SaveListService {

    @Inject
    private TodoListRepository todoListRepository;

    @Transactional
    public TodoList save(final TodoList list) {
        todoListRepository.saveOrUpdate(list);
        return list;
    }
}
