package net.eldiosantos.spark.lists.service;

import net.eldiosantos.spark.lists.model.TodoList;
import net.eldiosantos.spark.lists.repository.TodoListRepository;

import javax.inject.Inject;
import java.util.List;

/**
 * Retrieves lists from database.
 */
public class GetListService {

    @Inject
    private TodoListRepository todoListRepository;

    public TodoList get(final Long listId) {
        return todoListRepository.getByPk(listId);
    }

    public List<TodoList> list() {
        return todoListRepository.listAll();
    }
}
