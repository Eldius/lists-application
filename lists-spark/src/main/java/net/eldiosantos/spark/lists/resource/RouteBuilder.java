package net.eldiosantos.spark.lists.resource;

/**
 * Router configurator interface.
 */
public interface RouteBuilder {
    public void configure();
}
