package net.eldiosantos.spark.lists.resource;

import com.google.gson.Gson;
import net.eldiosantos.spark.lists.model.TodoList;

import net.eldiosantos.spark.lists.service.GetListService;
import net.eldiosantos.spark.lists.service.SaveListService;

import javax.inject.Inject;

import static spark.Spark.*;

/**
 * List routes configurator.
 */
public class ListRoutes  implements RouteBuilder{

    @Inject
    private SaveListService saveListService;

    @Inject
    private GetListService getListService;

    @Override
    public void configure() {
        final Gson gson = new Gson();

        // List all lists
        get("/list", (req, res) -> getListService.list(), gson::toJson);

        // Get a specific list
        get("/list/:id", (req, res) -> getListService.get(Long.parseLong(req.params(":id"))), gson::toJson);

        // Save a list
        post("/list", (req, res) -> saveListService.save(gson.fromJson(req.body(), TodoList.class)), gson::toJson);
    }
}
