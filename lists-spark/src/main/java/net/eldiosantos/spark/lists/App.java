package net.eldiosantos.spark.lists;

import com.google.gson.Gson;
import javassist.NotFoundException;
import net.eldiosantos.spark.lists.resource.RouteBuilder;
import org.jboss.weld.environment.se.events.ContainerInitialized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Spark;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import java.io.IOException;

import static spark.Spark.*;

/**
 * Hello world!
 *
 */
public class App 
{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    @Any
    protected Instance<RouteBuilder> builders;

    public void main(@Observes ContainerInitialized event) {

        try{
            logger.debug(String.format("Route builders found:\n%s", builders.toString()));

            final Gson gson = new Gson();

            port(8080);

            builders.forEach(RouteBuilder::configure);

            after((request, response) -> {
                response.type("application/json");
            });

            // STOP application.
            get("/stop", (req, res) -> {
                stop();
                halt(204, "System shut down...");
                return "";
            });

            // TEST resource
            get("/test"
                    , (req, res) -> {
                        res.status(418);
                        return "Ooops... I'm a teapot";
                    }
                    , gson::toJson
            );

            exception(NotFoundException.class, (e, request, response) -> {
                response.status(404);
                response.body("Ooops, you tryied to access a bad stuff (I think... I hope... whatever)... How about try again with another thing?");
            });

            new Thread(() -> {
                try {
                    System.in.read();
                    stop();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

        } catch (Exception e) {
            logger.error("Error setting routes", e);
        }
    }
}
